import { Meteor } from 'meteor/meteor';

import methods from '../api/methods';

Meteor.methods(methods);
