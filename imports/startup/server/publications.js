import { Meteor } from 'meteor/meteor';
import Decks from '../../collections/decks';

Meteor.publish('decksPublication', () => Decks.find({}));
