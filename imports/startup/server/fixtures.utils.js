import r from 'ramda';

export const vsName = r.compose(
  r.join(''),
  r.intersperse(' Vs ')
);

export const makeColumns = r.map(r.compose(
  r.concat(r.__, [0]),
  r.of,
));

const survey = r.unapply(r.zipObj(['name', 'columns']));
export const makeSurvey = r.converge(survey, [vsName, makeColumns]);
