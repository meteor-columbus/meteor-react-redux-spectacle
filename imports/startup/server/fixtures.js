import { map } from 'ramda';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

import { makeSurvey } from './fixtures.utils';
import Decks from '../../collections/decks';

const controllerUserName = 'controller';

if (Meteor.users.find().count() === 0) {
  Accounts.createUser({
    username: controllerUserName,
    email: 'controller@email.com',
    password: 'password',
  });
}

if (Decks.find().count() === 0) {
  const { _id } = Meteor.users.findOne({ username: controllerUserName });

  Decks.insert({
    ownerUserId: _id,
    currentSlide: 0,
    lastSlideIndex: 27,
    surveys: map(makeSurvey, [
      ['apple', 'microsoft'],
      ['tabs', 'spaces', 'who cares'],
      ['vim', 'emacs', 'huh?'],
      ['Sega', 'Nintendo'],
      ['good', 'evil?'],
      ['react', 'angular'],
    ]),
  });
}
