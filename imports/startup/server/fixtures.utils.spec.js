import { expect } from 'chai';

import {
  shouldBeAnArray,
  shouldBeAnObject,
  shouldBeAString,
  shouldEqual,
  shouldHaveKeys,
  testIfExists,
} from 'how-the-test-was-won';

import { vsName, makeColumns, makeSurvey } from './fixtures.utils';

describe('Fixture Utils', () => {
  const arr = ['chocolate', 'peanut butter', 'marshmallows'];

  describe('#vsName', () => {
    describe('given a valid array of three strings', () => {
      const result = vsName(arr);
      const expected = `${arr[0]} Vs ${arr[1]} Vs ${arr[2]}`;

      testIfExists(result);
      shouldBeAString(result);
      shouldEqual(result, expected);
    });
  });

  describe('#makeColumns', () => {
    describe('given a valid array of three strings', () => {
      const result = makeColumns(arr);
      const expected = [
        [arr[0], 0],
        [arr[1], 0],
        [arr[2], 0],
      ];

      testIfExists(result);
      shouldBeAnArray(result);
      it('should return the expected array', () => {
        expect(result).to.deep.equal(expected);
      });
    });
  });

  describe('#makeSurvey', () => {
    describe('given a valid array of three strings', () => {
      const result = makeSurvey(arr);
      const expected = {
        name: vsName(arr),
        columns: makeColumns(arr),
      };

      testIfExists(result);
      shouldBeAnObject(result);
      shouldHaveKeys(result, 'name', 'columns');
      it('should return the expected object', () => {
        expect(result).to.deep.equal(expected);
      });
    });
  });
});
