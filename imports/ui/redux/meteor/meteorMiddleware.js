import { Meteor } from 'meteor/meteor';
import { actionMatch } from '../utils';
import { CALL_METHOD } from './index';

export default () => next => action => {
  if (actionMatch(CALL_METHOD, action)) {
    const { name, args } = action.payload;
    Meteor.call(name, ...args);
  }

  return next(action);
};
