import { createAction } from 'redux-actions';

export const CALL_METHOD = '@@meteor/callMethod';
export const callMethod = createAction(CALL_METHOD);
