import r from 'ramda';
import { createAction, handleActions } from 'redux-actions';

export const DECK_ALTERED = '@@deck/deckAltered';
export const deckAltered = createAction(DECK_ALTERED);

export const SET_DECK = '@@deck/setDeck';
export const setDeck = createAction(SET_DECK);
export const setDeckHandler = (state, { payload }) => r.merge(state, payload);

export const deckReducer = handleActions({ [SET_DECK]: setDeckHandler }, {});
