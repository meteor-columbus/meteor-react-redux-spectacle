import r from 'ramda';

export const deckLens = r.lensPath(['deck']);
export const getDeck = r.view(deckLens);
export const setDeck = r.set(deckLens);

const currentSlide = r.lensPath(['currentSlide']);
export const currentSlideLens = r.compose(deckLens, currentSlide);
export const getCurrentSlide = r.view(currentSlideLens);
export const setCurrentSlide = r.set(currentSlideLens);

export const notEqual = r.complement(r.equals);
