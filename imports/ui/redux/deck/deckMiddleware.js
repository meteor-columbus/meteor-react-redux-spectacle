/* globals Audio */
import { push } from 'react-router-redux';
import { actionMatch, notEqual } from '../utils';
import { getDeck, getCurrentSlide } from './actionUtils';
import { SET_DECK, DECK_ALTERED, setDeck } from './index';

const isDeckAlteredAction = actionMatch(DECK_ALTERED);
const isSetDeckAction = actionMatch(SET_DECK);

export default ({ getState, dispatch }) => next => action => {
  const state = getState();

  if (isDeckAlteredAction(action) && notEqual(action.payload, getDeck(state))) {
    dispatch(setDeck(action.payload));
  }

  const { currentSlide } = action.payload;
  if (isSetDeckAction(action) && notEqual(currentSlide, getCurrentSlide(state))) {
    const magic = new Audio('/assets/magic.mp3');
    magic.play();
    dispatch(push(`/${currentSlide}`));
  }

  return next(action);
};
