import r from 'ramda';
import { createAction, handleActions } from 'redux-actions';

import { callMethod } from '../meteor';

export const VOTED_FOR = '@@votes/votedFor';
export const votedFor = createAction(VOTED_FOR);
export const votedForHandler =
  (state, { payload }) => r.uniq([...state, payload]);

// This is a thunk :)
export function vote(name, index) {
  return dispatch => {
    dispatch(votedFor(name));
    dispatch(callMethod({ name: 'vote', args: [name, { index }] }));
  };
}

export const votesReducer = handleActions({ [VOTED_FOR]: votedForHandler }, []);
