import r from 'ramda';
import { createAction, handleActions } from 'redux-actions';

export const TOGGLE_LOGIN = '@@ui/toggleLogin';
export const toggleLogin = createAction(TOGGLE_LOGIN);
export const toggleLoginHandler = r.over(r.lensPath(['showLogin']), r.not);

export const uiReducer = handleActions({
  [TOGGLE_LOGIN]: toggleLoginHandler,
}, {
  showLogin: false,
});
