import {
  call,
  complement,
  compose,
  equals,
  isNil,
  lensPath,
  nthArg,
  path,
  set,
  useWith,
  view,
} from 'ramda';

export const exists = complement(isNil);

export const notEqual = complement(equals);

export const payload = lensPath(['payload']);

export const actionMatch = useWith(call, [equals, path(['type'])]);

export const getPayload = compose(view(payload), nthArg(1));

export const createHandler =
  lens => (state, { payload }) => set(lens, payload, state);
