import { createAction, handleActions } from 'redux-actions';
import { getPayload } from '../utils';

export const SET_USER = '@@user/setUser';
export const setUser = createAction(SET_USER);

export const userReducer = handleActions({ [SET_USER]: getPayload }, null);
