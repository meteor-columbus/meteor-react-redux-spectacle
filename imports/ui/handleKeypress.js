import { notEqual } from './redux/utils';
import { callMethod } from './redux/meteor';
import { toggleLogin } from './redux/ui';

export default ({ getState, dispatch }) => ({ keyCode, ctrlKey }) => {
  const {
    lastSlideIndex,
    currentSlide,
    _id,
  } = getState().deck;

  const args = [_id];

  switch (keyCode) {
    case 37: // down
    case 33: // left
      return notEqual(currentSlide, 0) && dispatch(callMethod({
        name: 'decrementSlide',
        args,
      }));

    case 39: // up
    case 34: // right
      return notEqual(currentSlide, lastSlideIndex) && dispatch(callMethod({
        name: 'incrementSlide',
        args,
      }));

    case 82: // r
      return ctrlKey && dispatch(callMethod({
        name: 'resetSurveys',
        args,
      }));

    case 76: // l
      return ctrlKey && dispatch(toggleLogin({}));

    default:
      return true;
  }
};
