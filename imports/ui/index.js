/* globals document, window */
import React from 'react';
import thunk from 'redux-thunk';
import { render } from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { IndexRedirect, Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer, routerMiddleware } from 'react-router-redux';
import { compose, defaultTo } from 'ramda';

import App from './app';
import Decks from '../collections/decks';
import deckMiddleware from './redux/deck/deckMiddleware';
import meteorMiddleware from './redux/meteor/meteorMiddleware';
import createKeypressHandler from './handleKeypress';
import PresentationWrapper from './components/PresentationWrapper';
import { deckReducer, deckAltered } from './redux/deck';
import { userReducer, setUser } from './redux/user';
import { votesReducer } from './redux/votes';
import { uiReducer } from './redux/ui';

Meteor.subscribe('decksPublication');

const devTools =
  window.devToolsExtension ? window.devToolsExtension() : f => f;

const middleware = applyMiddleware(
  routerMiddleware(browserHistory),
  thunk,
  deckMiddleware,
  meteorMiddleware
);

const store = createStore(
  combineReducers({
    ui: uiReducer,
    user: userReducer,
    deck: deckReducer,
    votes: votesReducer,
    routing: routerReducer,
  }),
  compose(
    middleware,
    devTools,
  ),
);

const history = syncHistoryWithStore(browserHistory, store);

Tracker.autorun(() => {
  const deck = Decks.findOne({});
  if (deck) store.dispatch(deckAltered(deck));
});

Tracker.autorun(() => {
  store.dispatch(setUser(defaultTo({}, Meteor.user())));
});

Meteor.startup(() => {
  window.addEventListener('keydown', createKeypressHandler(store));

  render(
    <Provider store={store}>
      <Router history={history}>
        <Route path="/" component={App} >
          <IndexRedirect to="/0" />
          <Route path=":slide" component={PresentationWrapper(history)} />
        </Route>
      </Router>
    </Provider>,
    document.getElementById('app'));
});
