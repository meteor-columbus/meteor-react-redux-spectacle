export default
`Meteor.publish('Lists.public', function() {
  return Lists.find({
    userId: { $exists: false },
  }, {
    fields: Lists.publicFields,
  });
});
`;
