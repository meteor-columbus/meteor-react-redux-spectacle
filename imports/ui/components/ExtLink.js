import React, { PropTypes } from 'react';
import { Link, Text } from 'mrrs-spectacle';

const ExtLink = ({ linkTo, text }) => (
  <Text>
    <Link rel="noopener noreferrer" href={linkTo} target="_blank" >{text}</Link>
  </Text>
);

ExtLink.propTypes = {
  linkTo: PropTypes.string,
  text: PropTypes.string,
};

export default ExtLink;
