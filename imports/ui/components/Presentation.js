import React, { PropTypes } from 'react';

import {
  Code,
  CodePane,
  Deck,
  Heading,
  Image,
  List,
  ListItem,
  Markdown,
  Slide,
  Spectacle,
  Text,
} from 'mrrs-spectacle';

// Import image preloader util
// import preloader from 'spectacle/lib/utils/preloader';

import createTheme from 'mrrs-spectacle/lib/themes/default';
import ExtLink from './ExtLink';
import SurveySlide from './SurveySlide';
import AccountsUIWrapper from './AccountsUIWrapper';

require('normalize.css');
require('mrrs-spectacle/lib/themes/default/index.css');

const theme = createTheme({ primary: '#ff4081' });

const Presentation = ({ history, showLogin, surveys, vote, votes, user }) => (
  <div>
    <Spectacle theme={theme} history={history}>
      <Deck transition={['zoom', 'slide']} transitionDuration={500}>

        {/* cover page */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={1} textColor="primary">
            Meteor 101
          </Heading>
          <Markdown>
            {`
  From Zero to Production Everywhere

  Justin Foley & Kyle Chamberlain

  October 18th, 2016
            `}
          </Markdown>
        </Slide>

        {/* who are we */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Who are we?
          </Heading>
          <Markdown>
            {`
  * The Meteor Columbus Meetup hosts
  * Partners at Modern Web (work with us!)
            `}
          </Markdown>
        </Slide>

        {/* magic */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={4} textColor="primary">
            I was promised magic!
          </Heading>
          <Text>magic.modweb.io</Text>
        </Slide>

        {/* what is Meteor */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary" >
            What is Meteor?
          </Heading>
          <Text textAlign="left">
            Full-stack JS framework for building connected client web and mobile
            apps built on node.js
          </Text>
          <Text textAlign="left">
            (it’s a fancy node.js app)
          </Text>
          <Markdown>{`
  *  Data over the wire
  *  Dev and build tool
  *  Full interoperability with node/javascript ecosystem
  *  **Out of the box** sockets and robust server-client data sync
            `}
          </Markdown>
        </Slide>

        {/* meteor stack*/}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={4} textColor="primary">
            Meteor Stack
          </Heading>
          <Image src="/assets/meteor-stack.png" />
        </Slide>

        {/* step zero */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Step Zero
          </Heading>
          <CodePane
            textAlign="left"
            lang="bash"
            source={'curl https://install.meteor.com/ | sh'}
            margin="20px auto"
          />
          <Text textAlign="left">Create and run a new project locally</Text>
          <CodePane
            textAlign="left"
            lang="bash"
            source={'meteor create myapp\ncd myapp\nmeteor'}
            margin="20px auto"
          />
        </Slide>

        {/* meteor tool */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={4} textColor="primary">
            Meteor tool
          </Heading>
          <Markdown style={{ fontSize: 12 }}>
            {`
  * Like a **zero config** _Gulp/Webpack/Browsersync/Nodemon_ build pipeline + extras
  * Built on widely used nodejs tools like Babel
  * Reloads app on file changes
  * Compiles with build plugins (like ES2015 syntax/modules)
  * Concats, minifies, and bundles code for server and client
  * CSS/CSS-preprocessors
  * Atmosphere/npm packages
            `}
          </Markdown>
        </Slide>

        {/* Cordova Integration */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={4} textColor="primary">
            Cordova Integration
          </Heading>
          <Text textAlign="left" lineHeight={1.5}>
            Don't waste time wrestling cordova
          </Text>
          <Text textAlign="left" lineHeight={1.5}>
            <Code>meteor add-platform ios</Code>
          </Text>
          <Text textAlign="left" lineHeight={1.5}>
            <Code>meteor add-platform android</Code>
          </Text>
          <Text textSize=".8em" textAlign="left" lineHeight={1.5}>
            <Code textSize="1em">meteor run [ios|ios-device|android|android-device]</Code>
          </Text>
        </Slide>

        {/* livequery */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Livequery/DDP & Apollo
          </Heading>
          <Text textAlign="left">Monitors publications and pushes changes automatically</Text>
          <List>
            <ListItem>Livequery - reactive mongo queries</ListItem>
            <ListItem>Apollo - GraphQL data stack</ListItem>
            <ListItem>DDP, Distributed Data Protocol - REST for websockets</ListItem>
          </List>
        </Slide>

        {/* create a slide for each survey */}
        {surveys
            ? surveys.map((survey, i) => SurveySlide({ ...survey, user, vote, votes }, i))
            : null}

        {/* scalability */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Scaling Meteor
          </Heading>
          <Text textAlign="center">Connected clients and livequery make instances work hard</Text>
        </Slide>

        {/* mongo indexes */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Mongo Indexes
          </Heading>
          <CodePane
            textAlign="left"
            lang="js"
            source={'Posts._ensureIndex({ userId: 1 });'}
            margin="20px auto"
          />
        </Slide>

        {/* MongoDB Oplog */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Mongo Oplog
          </Heading>
          <Markdown>
            {`
  * Efficiently compute change sets to send connected clients
  * Oplog CPU efficiency > poll and diff
  * Mergebox can be memory intensive (more later)
            `}
          </Markdown>
        </Slide>


        {/* tune oplog */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Tune Oplog utilization
          </Heading>
          <Markdown>
            {`
  * METEOR_OPLOG_TOO_FAR_BEHIND (default 2000)
  * high write loads hinder oplog performance
  * fallback to poll and diff
            `}
          </Markdown>
        </Slide>

        {/* split write heavy collection */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Selectively use Oplog
          </Heading>
          <Text textAlign="left">Segregate write heavy collection, poll and diff</Text>
          <CodePane
            textAlign="left"
            lang="js"
            source={'Posts.find({\n\
  sort: { createdAt: -1 },\n\
  disableOplog: true, // force disable oplog\n\
  pollingIntervaMs: 1000, // interval in ms\n\
  pollingThrottleMs: 50, // minimum wait time between polls\n\
})'}
            margin="20px auto"
          />
        </Slide>

        {/* disable reactivity */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Disable Reactivity
          </Heading>
          <Text textAlign="left">If you don't need it, don't use it</Text>
          <CodePane
            textAlign="left"
            lang="js"
            source={'Posts.find({\n\
  sort: { createdAt: -1 },\n\
  reactive: false,\n\
})'}
            margin="20px auto"
          />
        </Slide>

        {/* minimum data */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Send the minimum data necessary
          </Heading>
          <CodePane
            textAlign="left"
            lang="js"
            source={'Posts.find({\n\
  limit: 10\n\
  fields: { createdAt: 1, content: 1, author: 1}\n\
  sort: { createdAt: -1 }\n\
})'}
            margin="20px auto"
          />
        </Slide>

        {/* disable mergebox */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Disable MergeBox
          </Heading>
          <Markdown>
            {`
  * mergebox helps minimize data sent to clients via DDP
  * on-server cache of client's data view, _per connection_
  * https://github.com/peerlibrary/meteor-control-mergebox
            `}
          </Markdown>
        </Slide>

        {/* kadira.io */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Kadira.io
          </Heading>
          <Markdown>
            {`
  * real-time performance monitoring and debugging
  * Performance traces, system profiler, alerts
  * Event stream, DDP timeline, server errors, client errors
  * https://kadira.io
            `}
          </Markdown>
        </Slide>

        {/* FastRender */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            FastRender
          </Heading>
          <Markdown>
            {`
* like server side rendering
* bundles initial payload with publication data
* https://github.com/kadirahq/fast-render
            `}
          </Markdown>
        </Slide>

        {/* CDN */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Static assets over CDN
          </Heading>
          <Text textAlign="left">App servers should serve domain data, not assets</Text>
          <CodePane
            textAlign="left"
            lang="js"
            source={'WebAppInternals.setBundledJsCssPrefix("http://mycdn.com")})'}
            margin="20px auto"
          />
        </Slide>

        {/* modifications: sound on slide change / Q&A */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Q & A
          </Heading>
          <ExtLink linkTo="mailto:contact@modweb.io" text="contact@modweb.io" />
        </Slide>

        {/* Resources */}
        <Slide transition={['slide']} bgColor="tertiary">
          <Heading caps size={2} textColor="primary">
            Resources
          </Heading>
          <ExtLink linkTo="https://gitlab.com/meteor-columbus/meteor-react-redux-spectacle" text="this slide deck" />
          <ExtLink linkTo="https://guide.meteor.com" text="guide.meteor.com" />
          <ExtLink linkTo="https://docs.meteor.com" text="docs.meteor.com" />
          <ExtLink linkTo="https://forums.meteor.com" text="forums.meteor.com" />
          <ExtLink linkTo="https://transmission.simplecast.fm" text="transmission.simplecast.fm" />
        </Slide>

      </Deck>
    </Spectacle>
    { showLogin ? <AccountsUIWrapper /> : null}
  </div>
);

Presentation.propTypes = {
  callMethod: PropTypes.func,
  showLogin: PropTypes.bool,
  history: PropTypes.object,
  surveys: PropTypes.array,
  user: PropTypes.object,
  votes: PropTypes.array,
  vote: PropTypes.func,
};

export default Presentation;
