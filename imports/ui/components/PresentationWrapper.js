import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Presentation from './Presentation';
import { callMethod } from '../redux/meteor';
import { vote } from '../redux/votes';

export default history => {
  const PresentationWrapper = ({ callMethod, showLogin, surveys, votes, user, vote }) => (
    <Presentation
      history={history}
      callMethod={callMethod}
      showLogin={showLogin}
      surveys={surveys}
      votes={votes}
      user={user}
      vote={vote}
    />
  );

  PresentationWrapper.propTypes = {
    callMethod: PropTypes.func,
    showLogin: PropTypes.bool,
    surveys: PropTypes.array,
    votes: PropTypes.array,
    user: PropTypes.object,
    vote: PropTypes.func,
  };

  const mapStateToProps = ({ ui, deck, votes, user }) => ({
    user,
    votes,
    showLogin: ui.showLogin,
    surveys: deck.surveys,
  });

  const mapDispatchToProps =
    dispatch => bindActionCreators({ callMethod, vote }, dispatch);

  return connect(mapStateToProps, mapDispatchToProps)(PresentationWrapper);
};
