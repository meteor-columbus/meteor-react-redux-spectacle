import r from 'ramda';
import C3Chart from 'react-c3js';
import React, { PropTypes } from 'react';
import { Heading, Slide } from 'mrrs-spectacle';

const SurveySlide = ({ name, columns, user, vote, votes }, i) => (
  <Slide key={i} transition={['slide']} bgColor="tertiary">
    <Heading caps size={2} textColor="primary">{name}</Heading>

    { r.isEmpty(user)
      ? <div>Cast your vote!</div>
      : <C3Chart
        legend={{ show: false }}
        data={{ columns, zoom: true, type: 'pie' }}
        pie={{
          label: {
            format: (value, id, index) => `${index} (${value})`,
          },
        }}
      />
    }

    {r.contains(name, votes) && r.isEmpty(user)
      ? <Heading
        style={{ margin: 20 }}
        textColor="primary"
        size={5}
        caps
      >Thanks for the vote :)</Heading>
      : columns.map(([id], i) => (
        <div key={id} className="btn btn-primary" onClick={() => vote(name, i)}>
          {id}
        </div>
      ))
    }
  </Slide>
);

SurveySlide.propTypes = {
  callMethod: PropTypes.func,
  columns: PropTypes.array,
  name: PropTypes.string,
  user: PropTypes.object,
  votes: PropTypes.array,
  vote: PropTypes.func,
};

export default SurveySlide;
