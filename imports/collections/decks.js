import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Mongo } from 'meteor/mongo';
import { Match } from 'meteor/check';

const SurveySchema = new SimpleSchema({
  name: {
    type: String,
  },
  columns: {
    type: Array,
  },
  'columns.$': {
    type: Match.Any,
  },
});

const DeckSchema = new SimpleSchema({
  currentSlide: {
    type: Number,
    defaultValue: 0,
  },
  ownerUserId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  lastSlideIndex: {
    type: Number,
  },
  surveys: {
    type: [SurveySchema],
  },
});

const Decks = new Mongo.Collection('decks');
Decks.attachSchema(DeckSchema);

export default Decks;
