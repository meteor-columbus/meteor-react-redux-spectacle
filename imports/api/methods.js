import r from 'ramda';

import { Meteor } from 'meteor/meteor';
import Decks from '../collections/decks';
import { resetSurveys } from './methods.utils';

const err = msg => {
  throw new Meteor.Error(msg);
};

export default {
  getVerifiedDeck(id) {
    const deck = Decks.findOne(id);

    if (!deck) {
      err(`Deck with _id of ${id} was not found.`);
    }

    if (deck.ownerUserId !== this.userId) {
      err('Calling user is not owner of deck and unauthorized to perform action');
    }

    return deck;
  },

  changeSlide(id, f) {
    const { currentSlide, lastSlideIndex } = Meteor.call('getVerifiedDeck', id);
    const newSlide = f(currentSlide);
    if (newSlide < 0 || newSlide > lastSlideIndex) return true;
    return Decks.update(id, {
      $set: {
        currentSlide: newSlide,
      },
    });
  },

  incrementSlide(id) {
    return Meteor.apply('changeSlide', [id, r.inc]);
  },

  decrementSlide(id) {
    return Meteor.apply('changeSlide', [id, r.dec]);
  },

  vote(surveyName, { index }) {
    const deck = Decks.findOne({});
    const surveyIndex = r.findIndex(r.propEq('name', surveyName), deck.surveys);

    const lens = r.compose(
      r.lensIndex(surveyIndex),
      r.lensPath(['columns']),
      r.lensIndex(index),
      r.lensIndex(1)
    );

    return Decks.update(deck._id, {
      $set: {
        surveys: r.over(lens, r.inc, deck.surveys),
      },
    });
  },

  resetSurveys(id) {
    const deck = Meteor.call('getVerifiedDeck', id);

    return Decks.update(id, {
      $set: {
        surveys: resetSurveys(deck),
      },
    });
  },
};
