import r from 'ramda';

const val = r.lensIndex(1);
const surveys = r.lensPath(['surveys']);
const columns = r.lensPath(['columns']);
const resetVals = r.map(r.over(columns, r.map(r.set(val, 0))));
export const resetSurveys = r.compose(resetVals, r.view(surveys));
