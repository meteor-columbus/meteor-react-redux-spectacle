import { expect } from 'chai';
import { testIfExists, shouldBeAnArray } from 'how-the-test-was-won';

import { resetSurveys } from './methods.utils';

describe('Method Utils', () => {
  describe('#resetSurveys', () => {
    describe('given a valid deck object', () => {
      const deck = {
        surveys: [{
          name: 'steve',
          columns: [['a', 1], ['b', 2], ['c', 3]],
        }, {
          name: 'peve',
          columns: [['d', 1], ['e', 2], ['f', 3]],
        }],
      };

      const result = resetSurveys(deck);

      testIfExists(result);
      shouldBeAnArray(result);
      it('should return all values to zero in the surveys', () => {
        expect(result).to.deep.equal([{
          name: 'steve',
          columns: [['a', 0], ['b', 0], ['c', 0]],
        }, {
          name: 'peve',
          columns: [['d', 0], ['e', 0], ['f', 0]],
        }]);
      });
    });
  });
});
