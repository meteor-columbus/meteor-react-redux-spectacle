<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Meteor React Redux Spectacle](#meteor-react-redux-spectacle)
- [Prerequisites](#prerequisites)
- [Run Locally](#run-locally)
- [Helpful Development Tools](#helpful-development-tools)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Meteor React Redux Spectacle

**MRRS**: MRRS, a Really Reactive Slidedeck.

# Prerequisites

Install Meteor ([more info](https://www.meteor.com/install))

```shell
curl https://install.meteor.com/ | sh
```

Install nodejs (we like to use Homebrew)

```shell
brew update && brew install node
```

Install `n` to manage node versions

```shell
npm i -g n
```

Use the latest LTS release

```shell
n lts
```

# Run Locally

Clone project

```shell
git clone git@gitlab.com:meteor-columbus/meteor-react-redux-spectacle.git
```

Move to project directory

```shell
cd meteor-react-redux-spectacle
```

Install dependencies

```shell
npm i
```

Start the app

```shell
npm start
```

Point your browser to [http://localhost:3000](http://localhost:3000)

# Running with local `spectacle` fork

Clone the forked repo (outside of project director)
```shell
git clone git@github.com:modweb/spectacle.git
```

Move to `spectacle` directory
```shell
cd spectacle
```

Create globally-installed symlink
```shell
npm link
```

Use the symlinked local version of the package, from project directory
```shell
npm link spectacle
```

In the `spectacle` directory, run `npm build:lib` or `npm build:lib` to see local changes.
TODO: which is it?


# Helpful Development Tools

[Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)

[React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)
